package com.java;

public class AtBashCipher implements Cipher {

    private char[] characterArray = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    @Override
    public String encode(String message) {
        String encodedMessage = "";
        for (int i = 0; i < message.length(); i++) {
            char currentCharacter = message.charAt((i));
            for (int j = 0; j < characterArray.length; j++) {
                if (characterArray[j] == Character.toLowerCase(currentCharacter)) {
                    char convertedCharacter = characterArray[characterArray.length - 1 - j];
                    if (Character.isUpperCase(currentCharacter)) {
                        convertedCharacter = Character.toUpperCase(convertedCharacter);
                    }
                    encodedMessage += convertedCharacter;
                }
            }
            if (currentCharacter == ' ') encodedMessage += ' ';
        }
        return encodedMessage;
    }

    @Override
    public String decode(String message) {
        return encode(message);
    }
}